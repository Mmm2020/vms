﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using VMS.Controllers;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting.Logging;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace VMS.Controllers.Tests
{
    [TestClass()]
    public class VehicleManagementControllerVehicleDataManagement
    {
        [TestMethod()]
        public void GetFetchVehicles()
        {
            
            VehicleManagementController vehicleManagementController = new VehicleManagementController();

            var vehicles = vehicleManagementController.Get();

            Assert.IsTrue(vehicles.Count() > 0);

        }

        [TestMethod()]
        public void CreateVehicleRecord()
        {
            VehicleManagementController vehicleManagementController = new VehicleManagementController();

            var vehicle = new Model.Car
            {
                Make = "Toyota",
                Model = "2004",

            };
            vehicleManagementController.CreateVehicleData(vehicle);

            var vehicles = vehicleManagementController.Get().ToList();

            bool isPassed = false;
            if (vehicles.Any(v => v.Make == "Toyota"))
            {
                isPassed = true;
            }

            Assert.IsTrue(isPassed);

        }
    }
}