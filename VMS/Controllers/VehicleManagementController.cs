﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using VMS.Model;

namespace VMS.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VehicleManagementController : ControllerBase
    {

        public List<Car> vehicleTypes = new List<Car>();
        
                
        [HttpGet]
        public IEnumerable<Car> Get()
        {
            var filePath = AppDomain.CurrentDomain.BaseDirectory + "VehcileDataFile.json";

            //If no vehilce info saved and no file created
            if (System.IO.File.Exists(filePath))
            {
                var jsonString = System.IO.File.ReadAllText(filePath);

                if (jsonString.Length > 0)
                {
                    try
                    {
                        vehicleTypes = JsonConvert.DeserializeObject<List<Car>>(jsonString);
                    }
                    catch (Exception)
                    {
                    }
                }
            }

            return vehicleTypes;
        }

        [HttpPost]
        public bool CreateVehicleData([FromBody] Car car)
        {
            var filePath = AppDomain.CurrentDomain.BaseDirectory + "VehcileDataFile.json";

            //If no car info saved and no file created
            if (System.IO.File.Exists(filePath))
            {
                var jsonString = System.IO.File.ReadAllText(filePath);

                if (jsonString.Length > 0)
                {
                    try
                    {
                        vehicleTypes = JsonConvert.DeserializeObject<List<Car>>(jsonString);
                    }
                    catch (Exception)
                    {
                    }
                }
            }

            // Appening car record to exsting record in Json file
            vehicleTypes.Add(car);
            var jsonValue = JsonConvert.SerializeObject(vehicleTypes);
           
            System.IO.File.WriteAllText(filePath, jsonValue.ToString());

            return true;
        }
    }
}
