using System;

namespace VMS.Model
{
   public class VehicleType
    {
        public string Make { get; set; }
        public string Model { get; set; }

    }
    public class Car:VehicleType
    {
        public string Type { get; set; }
       
        public string Engine { get; set; }
        public string Doors { get; set; }
        public string Wheels { get; set; }
        public string BodyType { get; set; }

    }
}
