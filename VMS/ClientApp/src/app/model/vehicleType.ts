export class VehicleType {
  type: string;
  make: string;
  model: string;
  engine: string;
  doors: string;
  wheels: string;
  bodytype: string;
}
