import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { VehicleType } from '../model/vehicleType'

@Injectable({
  providedIn: 'root'
})
export class SavedataService {


  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8' })
  };

  private baseUrl = "";
  constructor(private httpClient: HttpClient, @Inject('BASE_URL') baseURL: string) {
    this.baseUrl = baseURL + 'vehiclemanagement';
  }

  createVehicleData(vehicleType: VehicleType) {
    return this.httpClient.post<VehicleType>(this.baseUrl, vehicleType, this.httpOptions).subscribe(
      (val) => {
        console.log("POST call successful value returned in body",
          val);
      },
      response => {
        console.log("POST call in error", response);
      },
      () => {
        console.log("The POST observable is now completed.");
      });;
  }
}
