import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SavedataService } from '../service/savedata.service';
import { NotificationService } from '../service/notification.service'


@Component({
  selector: 'save-vehicle-info',
  templateUrl: './save-vehicle-info.html'
})
export class SaveVehicleInfoComponent implements OnInit {
  submitted = false;
  constructor(private savedataService: SavedataService, private notifyService: NotificationService) { }

  vehicleInfoForm: FormGroup;

  ngOnInit(): void {

    this.vehicleInfoForm = new FormGroup({
      'type': new FormControl('', [Validators.required]),
      'make': new FormControl('', [Validators.required]),
      'model': new FormControl('', [Validators.required]),
      'engine': new FormControl('',[Validators.required]),
      'doors': new FormControl('', [Validators.required]),
      'wheels': new FormControl('', [Validators.required]),
      'bodytype': new FormControl('', [Validators.required]),

    });
    }

 

  get type() { return this.vehicleInfoForm.get('type'); }
  get make() { return this.vehicleInfoForm.get('make'); }
  get model() { return this.vehicleInfoForm.get('model'); }
  get engine() { return this.vehicleInfoForm.get('engine'); }
  get doors() { return this.vehicleInfoForm.get('doors'); }
  get bodytype() { return this.vehicleInfoForm.get('bodytype'); }
  get wheels() { return this.vehicleInfoForm.get('wheels'); }





  
  showToasterSuccess() {
    this.notifyService.showSuccess("Data saved sucessfully", "VMS")
  }

  showToasterError() {
    this.notifyService.showError("Something is wrong", "VMS")
  }

  showToasterInfo() {
    this.notifyService.showInfo("This is info", "VMS")
  }

  showToasterWarning() {
    this.notifyService.showWarning("This is warning", "VMS")
  }

  onSubmit() {

    this.submitted = true;

    // stop here if form is invalid
    if (!this.vehicleInfoForm.invalid) {
      this.savedataService.createVehicleData(this.vehicleInfoForm.value);
      this.showToasterSuccess();
      this.submitted = false;
    }

    

  }

}



